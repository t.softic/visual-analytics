# Visual Analytics
* [X] add node info 
* [X] add input option for number of clusters in agglomerative
* [ ] add loading when app callback is being executed - not possible in dash
* [X] add error handling
* [X] adjust classes of nodes when clicked on create visualization after some node manipulation (plus/minus)
* [X] add legend - in the end
   - diamond - virtual node
   - star - important node
   - bigger virtual node - more children in that comunity
   - white node - main node of the whole graph
   - colors - different clusters
   - plus - node can be expanded
   - minus - node can be colapsed
* [X] agglomerative clustering over dataframe (take only columns that are in array 'attrs')
* [X] generate and return clustering and graph report from louvain 
* [X] generate clustering and graph report for agglomerative 
* [X] change shapes of nodes (expandable, virtual, ...)?
* [X] ???three phases of virtual nodes - (plus - expand all child nodes, star - expand important, minus - colapse)???
* [X] compute importance of a node and assign it to the field 'important' (important by page rank or other criteria...) 
* [X] add visualization for important nodes 
* [X] compute positions based on networkx layout and add them to the nodes as:
	node = {
		'data': {...},
        	'expandable': True,
        	'expanded': True,
        	'classes': '...',
        	'important': True,
		'position': {
			'x': latitue, 'y': longitude
		}
    }
* [X] change size of community node based on number of children
        * make styling for nodes that have more than m or n child nodes (more children - bigger node)
	        -> I created example with adding a border(this is inside "stylesheet.py"):
		{
        		'selector':'[num_children > 5]',
		        'style':{
            			'border-width': 1,
		                'border-color': '#000'
	                        #adapt size
		        }
	   }
