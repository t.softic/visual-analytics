edges = []
nodes = []

#uses global variable edges
def has_children(id):
    for edge in edges:
        try:
            source = edge['data']['source']
        except:
            source = ""
        if source == id:
            return True
    return False

#uses global variable edges
def get_children(id):
    children = []
    for edge in edges:
        try:
            source = edge['data']['source']
        except:
            source = ""
        if source == id:
            children.append(edge['data']['target'])
    return children

#uses global variable edges
def all_children_shown(id, elements):
    children = get_children(id)
    for ch in children:
        found = False
        for element in elements:
            if element['data']['id'] == ch:
                found = True
        if found == False:
            return False
    return True


#true if elements contain node
def contains_node(id, elements):
    for element in elements:
        if element['data']['id'] == id:
            return True
    return False

#true if elements contain an edge
def contains_edge(edge, elements):
    for element in elements:
        source = ""
        target = ""
        try:
            source = element['data']['source']
            target = element['data']['target']
        except:
            source = ""
            target = ""
        if source == edge['data']['source'] and target == edge['data']['target']:
            return True
    return False

#adding children nodes
def add_children(id, elements):
    child_ids = []
    child_nodes = []
    child_edges = []
    for edge in edges:
        if edge['data']['source'] == id:
            child_ids.append(edge['data']['target'])
            child_edges.append(edge)
            #if not contains_node(edge['data']['target'], elements):
            #    child_ids.append(edge['data']['target'])
            #if not contains_edge(edge, elements):
            #        child_edges.append(edge)
    for ch in child_ids:
        #for edge in edges:
        #    if edge['data']['target'] == ch and edge['data']['source'] != id:
        #        if contains_node(edge['data']['source'], elements):
        #            child_edges.append(edge)
        #    elif edge['data']['source'] == ch:
        #        if contains_node(edge['data']['target'], elements):
        #            child_edges.append(edge)
        for node in nodes:
            if node['expandable'] == True:
                node['classes'] = 'plus '+ str(node['community_id'])
            if node['data']['id'] == ch:
               child_nodes.append(node)
    return child_nodes+child_edges

#removing children nodes
def remove_children(id, elements):
    children = []
    remove_branches = []
    removed_nodes = []
    for element in elements:
        source =""
        try:
            source = element['data']['source']
        except:
            source = ""
        if source == id:
            children.append(element['data']['target'])
            remove_branches.append(element)

    for branch in remove_branches:
        elements.remove(branch)

    for ch in children:
        for element in elements:
            if element['data']['id'] == ch:
                elements = remove_children(ch, elements)
                removed_nodes.append(element['data']['id'])
                elements.remove(element)

    #for rn in removed_nodes:
    #    for element in elements:
    #        source = ""
    #        target = ""
    #        try:
    #            source = element['data']['source']
    #            target = element['data']['target']
    #        except:
    #            source = ""
    #            target = ""
    #        if target == rn or source == rn:
    #            elements.remove(element)
    return elements

"""
Part from Louvain method
    edges = nx.to_edgelist(graph)
    for edge in edges:
        edge_list.append({'data': {'source': edge[0], 'target': edge[1], 'edge_info': edge[2]}})
"""

#FUNCTION COPIED FROM HERE https://scikit-learn.org/stable/auto_examples/cluster/plot_agglomerative_dendrogram.html#sphx-glr-auto-examples-cluster-plot-agglomerative-dendrogram-py
def plot_dendrogram(model, **kwargs):
    # Create linkage matrix and then plot the dendrogram

    # create the counts of samples under each node
    counts = np.zeros(model.children_.shape[0])
    n_samples = len(model.labels_)
    for i, merge in enumerate(model.children_):
        current_count = 0
        for child_idx in merge:
            if child_idx < n_samples:
                current_count += 1  # leaf node
            else:
                current_count += counts[child_idx - n_samples]
        counts[i] = current_count

    linkage_matrix = np.column_stack([model.children_]).astype(float)

    # Plot the corresponding dendrogram
    dendrogram(linkage_matrix, **kwargs)


def draw_largest_component(graph):
    largest_subgraph = max(nx.connected_component_subgraphs(graph), key=len)

    #All drawing layouts that nx has are here. We can make a switch-case statement according to what the user chooses
    pos = nx.spring_layout(largest_subgraph, k=0.05)

    """
    I didn't experiment with the other parameters of the layouts, we should do this
    pos = nx.circular_layout(largest_subgraph)
    pos = nx.fruchterman_reingold_layout(largest_subgraph,k=0.05)
    pos = nx.spectral_layout(largest_subgraph)
    pos = nx.kamada_kawai_layout(largest_subgraph)
    pos = nx.rescale_layout(largest_subgraph) #AttributeError: 'MultiGraph' object has no attribute 'shape'
    pos = nx.shell_layout(largest_subgraph)
    """

    """It draws only """
    plt.figure(figsize=(20, 20))
    nx.draw(largest_subgraph, pos=pos, linewidths=0.3,edge_color='black', node_size=50, alpha=0.6, with_labels=True)
    nx.draw_networkx_nodes(largest_subgraph, pos=pos, node_size=50)
    plt.savefig('graphfinal.png')
    plt.show()

def draw_communities(partition):

    # drawing like in the online tutorial for louvain
    # TODO: Fix drawing with nicer colors
    size = float(len(set(partition.values())))
    size = float(len(set(partition.values())))

    """
        I didn't experiment with the other parameters of the layouts, we should do this
        pos = nx.spring_layout(graph,k=0.05)
        pos = nx.circular_layout(graph)
        pos = nx.fruchterman_reingold_layout(graph)
        pos = nx.spectral_layout(graph)
        pos = nx.kamada_kawai_layout(graph)
        pos = nx.rescale_layout(graph) #AttributeError: 'MultiGraph' object has no attribute 'shape'
        pos = nx.shell_layout(graph)
    """

    # count = 0.
    # for com in set(partition.values()):
    # count = count + 1.
    # list_nodes = [nodes for nodes in partition.keys()
    # if partition[nodes] == com]
    # nx.draw_networkx_nodes(graph, pos, list_nodes, node_size=20, node_color=str(count / size))

    # print(list_nodes)

    # nx.draw_networkx_edges(graph, pos)
    # plt.savefig('graphfinal.png')
    # plt.show()


### ADVANCED INFORMATION ABOUT THE GRAPH
### WE don't need this one
def advanced_graph_info(graph):
    largest_subgraph = max(nx.connected_component_subgraphs(graph), key=len)
    """An undirected graph is connected if, for every pair of nodes, there is a path between them. For that
    to happen, most of the nodes should have at least a degree of two, except for those denominated leaves 
    which have a degree of 1."""
    if nx.is_connected(graph):
        print("The graph is connected")
    else:
        print("The graph is not connected")
    print("There are: ", nx.number_connected_components(graph), "connected components in the graph")



@app.callback(Output('visualization', 'elements'),
              [Input('url', 'href')])
def display_page(href):
    global nodes
    global edges
    global starting_elements
    global graph_report
    global cluster_report
    global no_clicks_expand
    global no_clicks_visualize
    global data_frame
    if href is None:
        return []
    else:
        no_clicks_visualize = 0
        no_clicks_expand = 0
        data_frame = None

        nodes = []
        edges = []
        starting_elements = []

        graph_report = pd.DataFrame()
        cluster_report = pd.DataFrame()
        return []

def main():

    data = pd.read_csv("../../datasets/marvel_universe/hero-network.csv")
    #print(data.shape)
    #data = data_preprocess(data)
    #print(data.shape)
    node_list, edge_list, starting_elements, clustering_report, graph_report = louvain_community_detection(data,'hero1','hero2')
    print(graph_report)

    #agglomerative_clustering(data,['sepal_length','sepal_width','petal_length','petal_width'],3)
    # preprocess_agglomerative(data,['sepal_length','sepal_width','petal_length','petal_width'])
    #node_list,edge_list,starting_elements,clust_report,graph_report=louvain_community_detection(data,'hero1','hero2')

    #agglomerative_clustering(data,attrs,5)
    #graph_info(graph)
    #pagerank_calc(graph)
